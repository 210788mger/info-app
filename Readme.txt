My idea was to start the project by adding base functionality, then include the styling as inline. Then to understand how SASS works and
transfer the inline CSS into SASS.
I've created a Sign Up, and dashboard components.
The Dashboard component includes a News and Weather API call.
I've used Axios for API calls, react-router-dom for page navigation, and had to use material UI to align the components inside the Dashboard component.
I estimate the project completion at 40%, as there is a lot of refactoring, adding the Stock component and implementing the  responsive design.

Project Templates:
 https://www.dropbox.com/sh/5h7mts1ob10lwip/AAAFZmOGSfrn_eWJGizp0enva?dl=0