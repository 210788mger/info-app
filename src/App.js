import './App.css';
import SignUp from "./Components/SignUp"
import Dashboard from "./Components/Dashboard"
import Navbar from "./Components/Navbar"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

function App() {
  return (
    <div >
      <Router>
        <Navbar />
        <div>
          <Switch>
            <Route exact path="/">
              <SignUp />
            </Route>
            <Route path="/dashboard">
              <Dashboard />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
