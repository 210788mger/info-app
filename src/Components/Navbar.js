import React from "react";
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';

function Header() {
    return <header><h2 style={{
        margin: "0px",
        justifyContent: "space-between",
        display: "flex",
        alignItems: "center",
        backgroundColor: "#ff7b25",
        color: "white",
        paddingLeft: "5px",
        paddingRight: "5px"
    }}><MenuIcon/>N<CloseIcon/></h2></header>
}

export default Header;