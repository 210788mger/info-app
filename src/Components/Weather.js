import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useAxiosGet } from "../Hooks/HttpRequests"
import Loader from "../Components/Loader";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import WeeklyTab from "./WeeklyTab"

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    element: {
        padding: "8px",
        textAlign: 'center',
        color: "white",
        fontWeight: "bold"
    },
    hourlyData: {
        display: "flex",
        justifyContent: "space-around"
    }
}));

function Weather() {

    let content = null;
    const classes = useStyles();

    const lat = "33.441792";
    const lon = "-94.037689";
    const apiKey = "250bd3f1afc71882905edc36ca7d4cc4";
    const exclude = "minutely";
    const getCurrentHour = new Date().getHours();

    let url =
        "https://api.openweathermap.org/data/2.5/onecall?lat=" +
        lat +
        "&lon=" +
        lon +
        "&exclude=" +
        exclude +
        '&appid=' +
        apiKey;

    let wheater = useAxiosGet(url);


    if (wheater.loading) {
        content = <Loader />
    }

    if (wheater.error) {
        content = <p>There was an error, please refresh or try again later.</p>
    }

    if (wheater.data) {

        content =
            <div style={{backgroundColor:"#FFB236",     maxWidth: "1000px",
            height: "515px",
            marginTop: "8px"}}>
                <Grid container spacing={2}>
                    <Grid item md={3}>
                        <Typography className={classes.element}>
                            <img src={`http://openweathermap.org/img/w/${wheater.data.current.weather[0].icon}.png`} alt="wthr img" />
                        </Typography>
                        <Typography style={{textTransform:"capitalize"}}   className={classes.element}>
                            {wheater.data.current.weather[0].description}
                        </Typography>
                        <Typography   className={classes.element}>
                            {wheater.data.timezone.substring(wheater.data.timezone.indexOf('/') + 1)}
                        </Typography>
                    </Grid>
                    <Grid item md={1}>
                        <Typography className={classes.element}></Typography>
                    </Grid>
                    <Grid item md={8}>
                        <Typography className={classes.element}><WeeklyTab weeklyData={wheater.data.daily}/></Typography>
                    </Grid>
                    <Grid item md={12}>
                        <Typography className={classes.element}></Typography>
                    </Grid>
                    <Grid item md={12}>
                        <Typography className={`${classes.element} ${classes.hourlyData}`}>
                            {wheater.data.hourly.slice(0, 8).map((data, i) => {
                                return (
                                <Typography   key={i}>
                                    <Typography>{data.temp}</Typography>
                                    <Typography>{getCurrentHour + i}</Typography>
                                </Typography>
                                ) 
                            })}
                        </Typography>
                    </Grid>
                </Grid>
            </div>
    }

    return <Typography>{content}</Typography>

}


export default Weather