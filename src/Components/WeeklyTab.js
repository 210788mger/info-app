import React, { useState } from 'react';
import {Tabs, Tab, AppBar} from "@material-ui/core"
import Typography from '@material-ui/core/Typography';

function TabPanel(props) {
  const {children, value, index} = props;
  return (
  <div>{value===index && (<h1>{children}</h1>)}</div>
  )
}

function WheaterTab({weeklyData}) {

  const [value, setValue] = useState(0);
  
  const handleChange=(e,val) => {
    setValue(val)
  };
  

  let averageTemp = weeklyData.reduce((sume, el) => sume + el.temp.day, 0) / weeklyData.length;

    return (
        <Typography>
          <AppBar position="static" color="transparent" >
            <Tabs value={value} onChange={handleChange} aria-label="days weather tab">
              <Tab label="Today"/>
              <Tab label="Tommorow" />
              <Tab label="Week" />
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
          <Typography>{ weeklyData[0].temp.day}</Typography>
          </TabPanel>
          <TabPanel value={value} index={1}>
          <Typography>{ weeklyData[1].temp.day}</Typography>
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Typography>{Math.round(averageTemp * 100) / 100}</Typography>
          </TabPanel>
        </Typography>
      );
}


export default WheaterTab;