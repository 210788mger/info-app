import React from 'react';
import { useAxiosGet } from "../Hooks/HttpRequests"
import Loader from "../Components/Loader";
import { Typography, Grid } from '@material-ui/core';


function News() {

    let content = null;

    const apiKey = "d9182a04937c40f19316ec74a30459d6";

    let url =
        "https://newsapi.org/v2/top-headlines?country=us&pageSize=1&apiKey=" + apiKey;

    let wheater = useAxiosGet(url);

    if (wheater.loading) {
        content = <Loader />
    }

    if (wheater.error) {
        content = <Typography>There was an error, please refresh or try again later.</Typography>
    }

    if (wheater.data) {
        content = <div style={{
            display: "flex",
            justifyContent: "flex-end",
            flexDirection: "column",
            maxWidth: "1000px",
            height: "515px",
            backgroundColor: "#ccc",
            backgroundRepeat: "repeat", 
            color: "white", 
            backgroundImage: "url(" + wheater.data.articles[0].urlToImage + ")", 
            backgroundSize: "contain", 
        }}>
            <Typography>{wheater.data.articles[0].title}</Typography>
            <Grid
                container
                direction="row"
                justify="space-between"
                alignItems="center"
                spacing={3}
            >
                <Grid item md={2}><Typography>{wheater.data.articles[0].source.name}</Typography></Grid>
                <Grid item md={2}><Typography>20m ago</Typography></Grid>
                <Grid item md={3}></Grid>
                <Grid item md={3}><Typography>HEADLINES</Typography></Grid>
                <Grid item md={2}></Grid>
            </Grid>
        </div>

    }
    return <Typography>{content}</Typography>

}


export default News