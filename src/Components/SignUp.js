import React, { useState } from "react";
import ShortTextIcon from '@material-ui/icons/ShortText';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import {useHistory } from 'react-router-dom';

function SignUp() {

    const [userData, setUserData] = useState({
        name: '',
        email: '',
        passowrd: '',
    })

    const handleSubmit = (e) => {
        e.preventDefault();
        
    }


    const handleChange = (e) => {
        const { name, value } = e.target;

        setUserData(prevState => {
            return {
                ...prevState,
                [name]: value
            };
        })
    };

    const history = useHistory();

    return (
        <div style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            margin: "auto",
            textAlign: "center",
            background: "linear-gradient(180deg, rgba(224,120,81,1) 0%, rgba(169,81,45,1) 35%, rgba(62,37,15,1) 100%)",
            color: "white"
        }}>
            <h1 style={{ paddingLeft: "20px", letterSpacing: "4px", paddingTop: "185px" }}>CREATE ACCOUNT</h1>
            <form style={{ width: "250px", margin: "auto", paddingRight: "14px" }} autoComplete="off" onSubmit={handleSubmit}>

                <div className="input-icons">

                    <div className="icon-div"><ShortTextIcon /></div>
                    <input className="input-field"
                        type="text"
                        placeholder="Name" value={userData.name} name="name" onChange={handleChange} />
                </div>

                <div className="input-icons">
                    <div className="icon-div"><MailOutlineIcon fontSize="small" /></div>
                    <input className="input-field"
                        type="text"
                        value={userData.email} placeholder="Email" name="email" onChange={handleChange} />
                </div>

                <div className="input-icons">
                    <div className="icon-div"><LockOpenIcon /></div>
                    <input className="input-field"
                        type="password"
                        value={userData.password} placeholder="Password" name="passowrd" onChange={handleChange} />
                </div>

                <button className="input-button" type="submit"  onClick={() => history.push('/dashboard')} >Continue</button>

            </form>
        </div>
    );
}

export default SignUp;