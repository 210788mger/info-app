import React from "react"
import Wethear from "./Weather"
import News from "./News"
import { Grid } from '@material-ui/core';

function Dashboard() {
return (
<div>
    <Grid
        container
        direction="row"
        justify="center"
  alignItems="center"
    >
        <Grid item md={6} xs={12}><News/></Grid>
        <Grid item md={6} xs={12}><Wethear/></Grid>
    </Grid>
</div>
)}


export default Dashboard;